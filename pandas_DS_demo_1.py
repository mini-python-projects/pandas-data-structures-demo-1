import pandas
import numpy

print("#python pandas data structure examples#")

print("\n\n#sample Series DS heights_A# heights_A = pandas.Series({'s1':10,'s2':20}) ")
heights_A = pandas.Series({'s1':10,'s2':20})
print(heights_A)
print("\n#type of object heights_A# type(heights_A) -> " + str(type(heights_A)))
print("\n#shape of sample Series DS# heights_A.shape -> " + str(heights_A.shape))
print("\n#accessing data in Series DS using index# heights_A[0] -> " + str(heights_A[0]))
print("\n#accessing data in Series DS using rowid# heights_A['s1'] -> " + str(heights_A['s1']))

print("\n#accessing data in Series DS using get method and index# heights_A.get(1) -> " + str(heights_A.get(1)))
print("\n#accessing data in Series DS using get method and rowid# heights_A.get(1) -> " + str(heights_A.get('s2')))


print("\n\n#sample Series DS temp# temp = pandas.Series(28 + 10*numpy.random.randn(10)) ")
temp = pandas.Series(28 + 10*numpy.random.randn(10))
print(temp)
print("\n#accessing describe method of a Series DS # temp.describe() -> \n" + str(temp.describe()))

print("\n\n#sample Data Frame DS df# df = pandas.DataFrame({'temp':pandas.Series(28 + 10*numpy.random.randn(10)),'rain':pandas.Series(100 + 50*numpy.random.randn(10)),'location':list('AAAAABBBBB')} ")
df = pandas.DataFrame({'temp':pandas.Series(28 + 10*numpy.random.randn(10)), 
                'rain':pandas.Series(100 + 50*numpy.random.randn(10)),
             'location':list('AAAAABBBBB')})
print(df)
print("\n#type of object df# type(df) -> " + str(type(df)))
print("\n#shape of sample Data Frame DS# df.shape -> \n" + str(df.shape))
print("\n#info of sample Data Frame DS# df.info() -> \n" + str(df.info()))
print("\n#describe of sample Data Frame DS# df.describe() -> \n" + str(df.describe()))
print("\n#describe only object column of sample Data Frame DS# df.describe(include=['object']) -> \n" + str(df.describe(include=['object'])))
print("\n#describe only float64 column of sample Data Frame DS# df.describe(include=['float64']) -> \n" + str(df.describe(include=['float64'])))


print("\n\n#import sample data from web iris.csv#")
import urllib
print('''
myUrl = "http://aima.cs.berkeley.edu/data/iris.csv"\n
urlRequest = urllib.request.Request(myUrl)\n
iris_file = urllib.request.urlopen(urlRequest)\n
iris_df = pandas.read_csv(iris_file, sep=',', header=None, decimal='.',names=['sepal_length','sepal_width','petal_length','petal_width','target'])
''')
myUrl = "http://aima.cs.berkeley.edu/data/iris.csv"
urlRequest = urllib.request.Request(myUrl)
iris_file = urllib.request.urlopen(urlRequest)
iris_df = pandas.read_csv(iris_file, sep=',', header=None, decimal='.',names=['sepal_length','sepal_width','petal_length','petal_width','target'])
print("#sample data from iris_df# iris_df.head(6) -> \n" + str(iris_df.head(6)))
print("\n#type of object iris_df# type(iris_df) -> " + str(type(iris_df)))
print("\n#shape of sample Data Frame DS# iris_df.shape -> \n" + str(iris_df.shape))
print("\n#info of sample Data Frame DS# iris_df.info() -> \n" + str(iris_df.info()))
print("\n#describe of sample Data Frame DS# iris_df.describe() -> \n" + str(iris_df.describe()))
print("\n#describe only object column of sample Data Frame DS# iris_df.describe(include=['object']) -> \n" + str(iris_df.describe(include=['object'])))
print("\n#describe only float64 column of sample Data Frame DS# iris_df.describe(include=['float64']) -> \n" + str(iris_df.describe(include=['float64'])))


print("#\n\n#pandas JSON read & write#")
import json
EmployeeRecords = [{'EmployeeID':451621, 'EmployeeName':'Preeti Jain', 'DOJ':'30-Aug-2008'},
{'EmployeeID':123621, 'EmployeeName':'Ashok Kumar', 'DOJ':'25-Sep-2016'},
{'EmployeeID':451589, 'EmployeeName':'Johnty Rhodes', 'DOJ':'04-Nov-2016'}]

EmployeeRecords_json_str = json.dumps(EmployeeRecords)
print("\n#sample JSON string from JSON data# EmployeeRecords_json_str = json.dumps(EmployeeRecords) -> \n" + str(EmployeeRecords_json_str))
json_df = pandas.read_json(EmployeeRecords_json_str, orient='records', convert_dates=['DOJ'])
print("\n#sample JSON Data frame# json_df = pandas.read_json(EmployeeRecords_json_str, orient='records', convert_dates=['DOJ']) ->\n" + str(json_df))


print("\n\n#Indexing a Data Fame in pandas#")

sample_df = pandas.DataFrame(numpy.random.rand(5,2))
print("\n#sample DataFrame using numpy# sample_df = pandas.DataFrame(numpy.random.rand(5,2))\n" + str(sample_df))
sample_df.index = ["row_" + str(i) for i in range(1,6)]
print("\n#sample DataFrame using numpy & index# sample_df.index = ['row_' + str(i) for i in range(1,6)]\n" + str(sample_df))

